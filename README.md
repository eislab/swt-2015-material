# README #

Das Projekt swt-2015-material enthält Ausgangsmaterialien für die verbleibenden Java-Aufgaben der SWT Übungsblätter.

Das Hauptprojekt swt-2015-material ist lediglich zur Vereinfachung der GIT-Synchronisation im IntelliJ IDEA gedacht,
nicht aber zum Kompilieren & Ausführen der Unterprojekte (z.B. "uebungsblatt06"). Öffnen Sie dazu das jeweilige Projekt
des Übungsblatts, indem sie per "Open Project" den entsprechenden Unterordner (Z.B. "swt-2015-material/uebungsblatt06/")
auswählen. 

Denken Sie daran, dass Output-Files ("/out/", "/production/") NICHT mit abzugeben sind, sondern immer nur Quelldateien
("src") sowie mögliche Ressourcen ("assets") und Bibliotheken ("lib").