package controller;

import model.Answer;
import model.Game;
import model.Question;
import view.GUI;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.*;
import java.util.List;

public class GameController implements ActionListener {

    private Game game;
    private Question currentQuestion;
    private GUI gui;

    public GameController(Game game) {
        this.game = game;
    }

    public GameController() {
        List<Question> questions = null;
        boolean remoteError = false;
        try {
            questions = QuestionLoader.readQuestionsFromRemote(QuestionLoader.URL);
        } catch(FileNotFoundException e) {
            System.err.println("Die Datei an " + QuestionLoader.URL + " konnte nicht gefunden werden! Verwende lokale Fragenbibliothek...");
            remoteError = true;
        } catch (IOException e) {
            System.err.println("Es gab ein Problem beim Einlesen der Fragendatei. Bitte Existenz und Zugriffsrechte der Datei prüfen. Verwende lokale Fragenbibliothek...");
            remoteError = true;
        }
        // We were not able to download the remote CSV file and will use the local questions library instead
        if (remoteError) {
            try {
                questions = QuestionLoader.readQuestionsFromFile(QuestionLoader.LOCAL_FILE_NAME);
            } catch (IOException e) {
                System.err.println("Die lokale Fragenbibliothek " + QuestionLoader.LOCAL_FILE_NAME + " konnte ebenfalls nicht gelesen werden. Beende Programm...");
                System.exit(-1);
            }
        }
        this.game = new Game(questions);
    }

    public void play(GUI reusableGui) {
        System.out.println("Starte...");

        if (reusableGui == null && this.gui == null) {
            this.gui = new GUI("Wer Wird Millionär");
        } else {
            this.gui = reusableGui;
        }
        gui.addButtonListener(this);
        gui.setVisible(true);
        this.proceed();
    }

    private void proceed() {
        if (!game.istGewonnen()) {
            this.currentQuestion = this.game.nextQuestion();
            gui.showQuestionAndAnswers(currentQuestion);
        } else {
            this.win();
        }
    }

    private void lose() {
        gui.showFinish("You Lose!");
    }

    private void win() {
        gui.showFinish("You Win!");
    }

    public static void main(String args[]) {
        GameController controller = new GameController();
        controller.play(null);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        String action = e.getActionCommand();
        int answerID = -1;
        if (action.equalsIgnoreCase(GUI.ACTION_A)) {
            answerID = 0;
        } else if (action.equalsIgnoreCase(GUI.ACTION_B)) {
            answerID = 1;
        } else if (action.equalsIgnoreCase(GUI.ACTION_C)) {
            answerID = 2;
        } else if (action.equalsIgnoreCase(GUI.ACTION_D)) {
            answerID = 3;
        }
        if (answerID <= this.currentQuestion.getAnswers().size() - 1) {
            Answer answerAttempt = this.currentQuestion.getAnswers().get(answerID);
            if (this.currentQuestion.isCorrectAnswer(answerAttempt)) {
                this.game.richtigBeantwortet();
                this.proceed();
            } else {
                this.lose();
            }
        } else if (answerID == -1) {
            this.lose();
        }

    }
}
