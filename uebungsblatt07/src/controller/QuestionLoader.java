package controller;

import com.opencsv.CSVReader;
import model.Answer;
import model.Question;

import java.io.*;
import java.net.URL;
import java.nio.channels.Channels;
import java.nio.channels.ReadableByteChannel;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class QuestionLoader {


    public static final String DOWNLOADED_FILE_NAME = "questions-remote.csv";
    public static final String URL = "http://www.eislab.fim.uni-passau.de/lehrmaterial/questions.csv";
    public static final String LOCAL_FILE_NAME = "uebung06" + File.separator + "assets" + File.separator + "questions.csv";

    public static List<Question> readQuestionsFromFile(String filename) throws FileNotFoundException, IOException {
        // FileReader zum Lesen der csv-Datei anlegen.
        Reader fileReader = new FileReader(filename);
        // CSVReader um den FileReader "wrappen". Trennungszeichen ist das Komma
        CSVReader csvReader = new CSVReader(fileReader, ',');
        // Eine Liste aus Zeilen (=String-Array) vom CSVReader zurückgeben lassen
        List<String[]> lines = csvReader.readAll();
        // Leere Liste zur Befüllung mit Fragen anlegen
        List<Question> questions = new ArrayList<Question>();
        // Durch alle Zeilen iterieren
        for (String[] line : lines) {
            // Erste Antwort ist die richtige Antwort
            Answer correctAnswer = new Answer(line[1]);
            // Alle weiteren Antworten kommen mit der Richtigen zusammen in die Sammlung
            List<Answer> allAnswers = new ArrayList<Answer>();
            allAnswers.add(correctAnswer);
            for (int index = 2; index < line.length; index++) {
                allAnswers.add(new Answer(line[index]));
            }
            // Frage erstellen
            questions.add(new Question(line[0], correctAnswer, allAnswers));
        }
        // Fragen mischen und zurückgeben
        Collections.shuffle(questions);
        return questions;
    }

    public static List<Question> readQuestionsFromRemote(String urlString) throws IOException {
        downloadFile(urlString);
        return readQuestionsFromFile(DOWNLOADED_FILE_NAME);
    }

    private static void downloadFile(String url) throws IOException {
        URL website = new URL(url);
        ReadableByteChannel byteChannel = Channels.newChannel(website.openStream());
        FileOutputStream outputStream = new FileOutputStream(DOWNLOADED_FILE_NAME);
        outputStream.getChannel().transferFrom(byteChannel, 0, Long.MAX_VALUE);
    }
}
