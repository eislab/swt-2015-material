package controller;

import model.Answer;
import model.Game;
import model.Question;
import view.GUI;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.List;

public class GameController implements ActionListener {

    private static int SLEEP_DURATION = 1000; // = 1 Sekunde
    private static String WINDOW_TITLE = "Wer wird Millionär";
    private static String WIN_MESSAGE = "YOU WIN!";
    private static String LOSE_MESSAGE = "YOU LOSE!";

    private Game game;
    private Question currentQuestion;
    private GUI gui;
    private Thread delayThread;

    public GameController(Game game) {
        this.game = game;
    }

    public GameController() {
        this.initGame();
    }

    private void initGame(){
        List<Question> questions = null;
        boolean remoteError = false;
        try {
            questions = QuestionLoader.readQuestionsFromRemote(QuestionLoader.URL);
        } catch(FileNotFoundException e) {
            System.err.println("Die Datei an " + QuestionLoader.URL + " konnte nicht gefunden werden!");
            remoteError = true;
        } catch (IOException e) {
            System.err.println("Es gab ein Problem beim Einlesen der Fragendatei. Bitte Existenz und Zugriffsrechte der Datei prüfen.");
            remoteError = true;
        }
        // We were not able to download the remote CSV file and will use the local questions library instead
        if (remoteError) {
            try {
                QuestionLoader.readQuestionsFromFile(QuestionLoader.LOCAL_FILE_NAME);
            } catch (IOException e) {
                System.err.println("Die lokale Fragenbibliothek " + QuestionLoader.LOCAL_FILE_NAME + " konnte ebenfalls nicht gelesen werden. Beende Programm...");
                System.exit(-1);
            }
        }
        this.game = new Game(questions);
    }

    public void play(GUI reusableGui) {
        if (reusableGui == null && this.gui == null) {
            this.gui = new GUI(WINDOW_TITLE);
            gui.addWindowListener(new WindowListener() {
                @Override
                public void windowClosing(WindowEvent e) {
                    int result = JOptionPane.showConfirmDialog(gui, "Wirklich beenden?", "Programm beenden", JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE);
                    if (result == JOptionPane.YES_OPTION) {
                        System.exit(0);
                    }
                }

                @Override
                public void windowOpened(WindowEvent e) {
                }
                @Override
                public void windowClosed(WindowEvent e) {
                }
                @Override
                public void windowIconified(WindowEvent e) {
                }
                @Override
                public void windowDeiconified(WindowEvent e) {
                }
                @Override
                public void windowActivated(WindowEvent e) {
                }
                @Override
                public void windowDeactivated(WindowEvent e) {
                }
            });
        } else {
            this.gui = reusableGui;
            if (this.gui != null) {
                this.gui.initGUI();
            }
        }
        if (gui != null) {
            gui.addButtonListener(this);
            gui.setVisible(true);
            gui.setJoker5050Status(true);
            this.proceed();
        }
    }

    private void proceed() {
        if (!game.istGewonnen()) {
            this.currentQuestion = this.game.nextQuestion();
            gui.showQuestionAndAnswers(currentQuestion);
        } else {
            this.win();
        }
    }

    private void lose() {
        gui.showFinish(LOSE_MESSAGE);
    }

    private void win() {
        gui.showFinish(WIN_MESSAGE);
    }

    public static void main(String args[]) {
        GameController controller = new GameController();
        controller.play(null);
    }

    private void resolveAnswer(Answer answerAttempt){
        if (this.currentQuestion.isCorrectAnswer(answerAttempt)) {
            this.gui.enterCorrectAnswerState(answerAttempt);
            this.game.richtigBeantwortet();
            this.delayThread = new Thread(){
                @Override
                public void run() {
                    super.run();
                    try {
                        sleep(SLEEP_DURATION);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    proceed();
                }
            };
        } else {
            this.gui.enterIncorrectAnswerState(this.currentQuestion.getCorrectAnswer(),answerAttempt);
            this.delayThread = new Thread(){
                @Override
                public void run() {
                    super.run();
                    try {
                        sleep(SLEEP_DURATION);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    lose();
                }
            };
        }
        delayThread.start();
    }

    private void apply5050Joker() {
        this.gui.setJoker5050Status(false);
        List<Answer> answers = this.currentQuestion.getAnswers();
        int disabledCount = 0;
        for(Answer answer : answers){
            if(!currentQuestion.isCorrectAnswer(answer)){
                this.gui.removeAnswerButton(answer);
                disabledCount++;
            }
            if(disabledCount >= 2){
                break;
            }
        }
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        String action = e.getActionCommand();
        JButton clickedButton = (JButton) e.getSource();

        // Wurde einer der Antwortbuttons geklickt?
        int answerID = -1;
        if (action.equalsIgnoreCase(GUI.ACTION_A)) {
            answerID = 0;
        } else if (action.equalsIgnoreCase(GUI.ACTION_B)) {
            answerID = 1;
        } else if (action.equalsIgnoreCase(GUI.ACTION_C)) {
            answerID = 2;
        } else if (action.equalsIgnoreCase(GUI.ACTION_D)) {
            answerID = 3;
        }

        // Wurde ein Neustart des Spiels angefordert?
        else if (action.equals(GUI.ACTION_RESTART)){
            this.initGame();
            this.play(this.gui);
            return;
        }

        // Wurde der 50:50 Joker eingesetzt?
        else if(action.equals(GUI.ACTION_5050_JOKER)){
            apply5050Joker();
            return;
        }

        // Hier kommen wir nur an, falls regulär geantwortet wurde. Deaktiviere Buttons und logge Antwort ein...
        this.gui.enterWaitState(this.currentQuestion.getAnswers().get(answerID));

        if (answerID <= this.currentQuestion.getAnswers().size() - 1) {
            final Answer answerAttempt = this.currentQuestion.getAnswers().get(answerID);
            // Dynamisch einen neuen Thread anlegen und direkt starten. Dazu muss run() überschrieben werden
            new Thread(){
                @Override
                public void run() {
                    try {
                        sleep(SLEEP_DURATION);
                        resolveAnswer(answerAttempt);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }.start(); // Der Thread wird nach dem Anlegen sofort aufgerufen
        }

        // Dieser Fall sollte nicht eintreten können (keine gültige Antwortauswahl) und wird hier lediglich abgesichert
        else if (answerID == -1) {
            new Thread(){
                @Override
                public void run() {
                    try {
                        sleep(SLEEP_DURATION);
                        lose();
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }.start();

        }
    }
}
