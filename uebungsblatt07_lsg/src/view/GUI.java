package view;

import model.Answer;
import model.Question;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionListener;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class GUI extends JFrame {
    private static final int FRAMESIZE = 400;
    public static final String ACTION_A = "A: ", ACTION_B = "B: ", ACTION_C = "C: ", ACTION_D = "D: ";
    public static final String ACTION_5050_JOKER = "50:50", ACTION_RESTART = "Neustarten";
    private JButton aButton, bButton, cButton, dButton; // GUI elements should be private
    private JButton jokerButton5050, restartButton;
    private JTextArea questionField;
    private List<JButton> answerButtons;
    private boolean joker5050available = true; // should be set to false when joker has been used, to true when game starts

    public GUI(String title) {
        super(title);
        this.initGUI();
    }

    public GUI(String title, int width, int height) {
        super(title);
        this.initGUI();
        this.setSize(width, height);
    }

    /**
     * used in _all_ constructors to perform standard settings to the GUI
     */
    public void initGUI() {
        this.setSize(FRAMESIZE, FRAMESIZE);
        this.setDefaultCloseOperation(WindowConstants.DO_NOTHING_ON_CLOSE);
        Container contentPane = this.getContentPane();
        contentPane.removeAll();
        contentPane.setLayout(new BorderLayout());
        contentPane.add(this.getQuestionPanel(), BorderLayout.NORTH);
        contentPane.add(this.getAnswerButtonPanel(), BorderLayout.SOUTH);
        contentPane.add(this.getAdditionalButtonsPanel(),BorderLayout.EAST);

        this.answerButtons = new ArrayList<JButton>();
        this.answerButtons.add(this.aButton);
        this.answerButtons.add(this.bButton);
        this.answerButtons.add(this.cButton);
        this.answerButtons.add(this.dButton);

        this.setResizable(false);
        this.setLocation(500, 500);
        contentPane.revalidate();
        this.pack();
    }

    /**
     * used by the controller to update the UI and show a new Question and its answers
     *
     * @param question the Question to be shown on the screen. It contains all possible answers, including the correct one
     */
    public void showQuestionAndAnswers(Question question) {
        List<Answer> answers = question.getAnswers();
        List<String> answerStrings = new ArrayList<String>(answers.size());
        for (Answer answer : answers) {
            answerStrings.add(answer.getText());
        }
        this.setQuestion(question.getText());
        this.setAnswerOptions(answerStrings);
        this.restartButton.setEnabled(true);
        this.jokerButton5050.setEnabled(joker5050available);
    }


    /**
     * used internally to initialize a button JPanel.
     *
     * @return a JPanel containing four buttons. The buttons use different ActionCommands to distinguish them
     */
    private JPanel getAnswerButtonPanel() {
        JPanel buttonPanel = new JPanel(new BorderLayout());
        JPanel northPanel = new JPanel();
        JPanel southPanel = new JPanel();


        // create the buttons and lay them out
        this.aButton = new JButton();
        this.bButton = new JButton();
        this.cButton = new JButton();
        this.dButton = new JButton();


        aButton.setActionCommand(ACTION_A);
        bButton.setActionCommand(ACTION_B);
        cButton.setActionCommand(ACTION_C);
        dButton.setActionCommand(ACTION_D);

        List<JButton> buttons = Arrays.asList(this.aButton, this.bButton, this.cButton, this.dButton);
        for (JButton button : buttons) {
            button.setPreferredSize(new Dimension(300, 200));
            button.setMinimumSize(new Dimension(300, 200));
            button.setOpaque(true);
        }

        northPanel.add(aButton);
        northPanel.add(bButton);
        southPanel.add(cButton);
        southPanel.add(dButton);

        buttonPanel.add(northPanel, BorderLayout.NORTH);
        buttonPanel.add(southPanel, BorderLayout.SOUTH);

        return buttonPanel;
    }

    /**
     * used to initialize the Question Panel
     *
     * @return a Panel containing a JTextField that displays the question.
     */
    private JPanel getQuestionPanel() {
        JPanel questionPanel = new JPanel();

        this.questionField = new JTextArea(3, 30);
        this.questionField.setLineWrap(true);
        this.questionField.setWrapStyleWord(true);
        this.questionField.setSize(FRAMESIZE, 100);
        this.questionField.setEditable(false); // we do not want people to edit the question
        this.questionField.setFont(new Font(Font.SANS_SERIF, Font.BOLD, 20));
        this.questionField.setBackground(questionPanel.getBackground());
        this.questionField.setBorder(null);
        questionPanel.add(this.questionField);
        return questionPanel;
    }


    private JPanel getAdditionalButtonsPanel(){
        JPanel additionalButtons = new JPanel();
        this.jokerButton5050 = new JButton(ACTION_5050_JOKER);
        this.jokerButton5050.setActionCommand(ACTION_5050_JOKER);

        this.restartButton = new JButton(ACTION_RESTART);
        restartButton.setActionCommand(ACTION_RESTART);

        additionalButtons.add(restartButton);
        additionalButtons.add(this.jokerButton5050);
        return additionalButtons;
    }

    /**
     * updates the text that is displayed in the question JTextField
     *
     * @param questionText the question to display
     */
    private void setQuestion(String questionText) {
        this.questionField.setText(questionText);
    }


    /**
     * updates a UI Button to display a letter and the answer text
     *
     * @param buttonNumber     the number of the button to be changed. 0 = A, 1 = B,...
     * @param answerOptionText the text to be displayed after the descriptive Letter (A, B,...)
     */
    private void setAnswerOption(int buttonNumber, String answerOptionText) {
        String buttonLabel = "";
        JButton button = null;
        switch (buttonNumber) {
            case 0:
                buttonLabel = "A: ";
                buttonLabel += answerOptionText;
                button = aButton;
                break;
            case 1:
                buttonLabel = "B: ";
                buttonLabel += answerOptionText;
                button = bButton;
                break;
            case 2:
                buttonLabel = "C: ";
                buttonLabel += answerOptionText;
                button = cButton;
                break;
            case 3:
                buttonLabel = "D: ";
                buttonLabel += answerOptionText;
                button = dButton;
                break;
        }
        button.setText(buttonLabel);
        button.setEnabled(true);
        button.setBackground(null);
    }

    /**
     * Convenience Method to update all Button texts
     *
     * @param option1 Answer A text
     * @param option2 Answer B text
     * @param option3 Answer C text
     * @param option4 Answer D text
     */
    private void setAnswerOptions(String option1, String option2, String option3, String option4) {
        this.setAnswerOption(0, option1);
        this.setAnswerOption(1, option2);
        this.setAnswerOption(2, option3);
        this.setAnswerOption(3, option4);
    }

    /**
     * Convenience method to update the text of all Buttons.
     *
     * @param options Button texts, i.e. answers, in order A, B, C, D
     */
    private void setAnswerOptions(List<String> options) {
        for (int i = 0; i < options.size(); i++) {
            if (i < 4) {
                this.setAnswerOption(i, options.get(i));
            }
        }
    }

    /**
     * adds an ActionListener to all Buttons. Buttons should have an action command set, that the ActionLister can rely on
     *
     * @param actionListener the listener instance
     */
    public void addButtonListener(ActionListener actionListener) {
        try{
            for(JButton answerButton : this.answerButtons){
                answerButton.addActionListener(actionListener);
            }
            this.jokerButton5050.addActionListener(actionListener);
            this.restartButton.addActionListener(actionListener);
        }
        catch (NullPointerException e){
            System.err.println("Leider konnte einem Button kein ActionListener hinzugefügt werden. Programm wird " +
                    "beendet");
            e.printStackTrace();
            System.exit(0);
        }

    }

    public void setJoker5050Status(boolean status) {
        this.jokerButton5050.setEnabled(status);
        this.joker5050available = status;
    }

    /**
     * "Removes" a button of an answer. Can be called if a Joker has been used.
     * @param answer
     */
    public void removeAnswerButton(Answer answer){
        JButton b = getButtonByAnswer(answer);
        b.setEnabled(false);
        b.setText("");
    }


    /**
     * can be called from outside to remove all content and show a message
     *
     * @param message the message to be displayed
     */
    public void showFinish(String message) {
        Container contentPane = this.getContentPane();
        contentPane.removeAll();

        restartButton.setEnabled(true);
        JTextField messageArea = new JTextField(message);
        messageArea.setBorder(null);
        messageArea.setFont(new Font(Font.SANS_SERIF, Font.BOLD, 50));
        messageArea.setEditable(false);
        messageArea.setHorizontalAlignment(JTextField.CENTER);
        contentPane.add(messageArea, BorderLayout.CENTER);
        contentPane.add(this.restartButton,BorderLayout.SOUTH);
        contentPane.revalidate();
    }

    /**
     * Colors the chosen answer button in ORANGE and disables all buttons.
     * @param chosenAnswer
     */
    public void enterWaitState(Answer chosenAnswer){
        disableAllAnswerButtons();
        jokerButton5050.setEnabled(false);
        restartButton.setEnabled(false);
        getButtonByAnswer(chosenAnswer).setBackground(Color.ORANGE);
    }

    /**
     * Colors the chosen answer button GREEN and re-enables
     * @param correctAnswer
     */
    public void enterCorrectAnswerState(Answer correctAnswer){
        getButtonByAnswer(correctAnswer).setBackground(Color.GREEN);
    }
    public void enterIncorrectAnswerState(Answer correctAnswer, Answer givenAnswer){
        if(givenAnswer.equals(correctAnswer)) return;
        getButtonByAnswer(correctAnswer).setBackground(Color.GREEN);
        getButtonByAnswer(givenAnswer).setBackground(Color.RED);
    }

    private JButton getButtonByAnswer(Answer answer) {
        for(JButton b : this.answerButtons){
            if(b.getText().contains(answer.getText())){
                return b;
            }
        }
        return null;
    }

    private void disableAllAnswerButtons() {
        for(JButton b : this.answerButtons){
            b.setEnabled(false);
        }
    }

}

