package model;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class Question {
    private String text;



    private Answer correctAnswer;
    private List<Answer> sortedAnswers;
    private List<Answer> shuffledAnswers;

    public Question(String text, Answer correctAnswer, Answer wrongAnswer1, Answer wrongAnswer2, Answer wrongAnswer3) {
        this.text = text;
        this.correctAnswer = correctAnswer;
        this.sortedAnswers = Collections.unmodifiableList(Arrays.asList(correctAnswer, wrongAnswer1, wrongAnswer2, wrongAnswer3));
        this.shuffledAnswers = Arrays.asList(correctAnswer, wrongAnswer1, wrongAnswer2, wrongAnswer3);
        Collections.shuffle(shuffledAnswers);
    }

    public Question(String text, Answer correctAnswer, List<Answer> answerOptions) {
        this.text = text;
        this.correctAnswer = correctAnswer;
        this.sortedAnswers = answerOptions;
        this.shuffledAnswers = new ArrayList<Answer>(answerOptions);
        Collections.shuffle(this.shuffledAnswers);
    }

    public String getText() {
        return this.text;
    }

    public List<Answer> getAnswers() { // dies gibt fortan nur noch die unsortierten Antworten zurueck
        return this.getAnswers(true);
    }

    public List<Answer> getAnswers(boolean gemischt) {
        if (gemischt) {
            return this.shuffledAnswers;
        } else {
            return this.sortedAnswers;
        }
    }

    /**
     * In manchen Spielen werden mehr oder weniger als 4 Antwortmoeglichkeiten benoetigt,
     * diese ist fuer diesen Fall vorgesehen.
     *
     * @param count die Anzahl der Antworten (inklusive der richtigen)
     * @return eine Liste, die die vorgegebene Anzahl an Antworten enthaelt, in zufaelliger Reihenfolge
     */
    public List<Answer> getAnswers(int count) {
        // sicherstellen, dass die gewuenschte Anzahl nicht groesser als die verfuegbaren Antwortmoeglichkeiten sind
        count = count > this.shuffledAnswers.size() ? this.shuffledAnswers.size() : count;

        List<Answer> result = new ArrayList<Answer>(count);
        for (int i = 0; i < count; i++) {
            result.add(shuffledAnswers.get(i));
        }
        return result;
    }

    public boolean isCorrectAnswer(Answer answerAttempt) {
        return this.correctAnswer.equals(answerAttempt);
    }

    public Answer getCorrectAnswer() {
        return correctAnswer;
    }
}
