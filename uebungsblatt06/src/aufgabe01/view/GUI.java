package aufgabe01.view;

import aufgabe01.model.Answer;
import aufgabe01.model.Question;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class GUI extends JFrame {
    private static final int FRAMESIZE = 400;
    public static final String ACTION_A = "A: ", ACTION_B = "B: ", ACTION_C = "C: ", ACTION_D = "D: ";
    private JButton aButton, bButton, cButton, dButton; // GUI elements should be private
    private JTextArea questionField;

    public GUI(String title) {
        super(title);
        this.commonInit();
    }

    public GUI(String title, int width, int height) {
        super(title);
        this.commonInit();
        this.setSize(width, height);
    }

    /**
     * used in _all_ constructors to perform standard settings to the GUI
     */
    private void commonInit() {
        this.setSize(FRAMESIZE, FRAMESIZE);
        this.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        Container contentPane = this.getContentPane();
        contentPane.setLayout(new BorderLayout());
        contentPane.add(this.getQuestionPanel(), BorderLayout.NORTH);
        contentPane.add(this.getButtonPanel(), BorderLayout.SOUTH);
        contentPane.setBackground(Color.LIGHT_GRAY);
        this.setResizable(false);
        this.setLocation(500, 500);
        this.pack();
    }

    /**
     * used by the controller to update the UI and show a new Question and its answers
     *
     * @param question the Question to be shown on the screen. It contains all possible answers, including the correct one
     */
    public void showQuestionAndAnswers(Question question) {
        List<Answer> answers = question.getAnswers();
        List<String> answerStrings = new ArrayList<String>(answers.size());
        for (Answer answer : answers) {
            answerStrings.add(answer.getText());
        }
        this.setQuestion(question.getText());
        this.setAnswerOptions(answerStrings);
    }

    /**
     * used internally to initialize a button JPanel.
     *
     * @return a JPanel containing four buttons. The buttons use different ActionCommands to distinguish them
     */
    private JPanel getButtonPanel() {
        JPanel buttonPanel = new JPanel(new BorderLayout());
        JPanel northPanel = new JPanel();
        JPanel southPanel = new JPanel();


        // create the buttons and lay them out
        this.aButton = new JButton();
        this.bButton = new JButton();
        this.cButton = new JButton();
        this.dButton = new JButton();


        aButton.setActionCommand(ACTION_A);
        bButton.setActionCommand(ACTION_B);
        cButton.setActionCommand(ACTION_C);
        dButton.setActionCommand(ACTION_D);

        List<JButton> buttons = Arrays.asList(this.aButton, this.bButton, this.cButton, this.dButton);
        for (JButton button : buttons) {
            button.setPreferredSize(new Dimension(300, 200));
            button.setMinimumSize(new Dimension(300, 200));
        }

        northPanel.add(aButton);
        northPanel.add(bButton);
        southPanel.add(cButton);
        southPanel.add(dButton);

        buttonPanel.add(northPanel, BorderLayout.NORTH);
        buttonPanel.add(southPanel, BorderLayout.SOUTH);

        return buttonPanel;
    }

    /**
     * used to initialize the Question Panel
     *
     * @return a Panel containing a JTextField that displays the question.
     */
    private JPanel getQuestionPanel() {
        JPanel questionPanel = new JPanel();
        this.questionField = new JTextArea(3, 30);
        this.questionField.setLineWrap(true);
        this.questionField.setWrapStyleWord(true);
        this.questionField.setSize(FRAMESIZE, 100);
        this.questionField.setEditable(false); // we do not want people to edit the question
        this.questionField.setFont(new Font(Font.SANS_SERIF, Font.BOLD, 20));
        this.questionField.setBackground(questionPanel.getBackground());
        this.questionField.setBorder(null);
        questionPanel.add(this.questionField);
        return questionPanel;
    }

    /**
     * updates the text that is displayed in the question JTextField
     *
     * @param questionText the question to display
     */
    private void setQuestion(String questionText) {
        this.questionField.setText(questionText);
    }


    /**
     * updates a UI Button to display a letter and the answer text
     *
     * @param buttonNumber     the number of the button to be changed. 0 = A, 1 = B,...
     * @param answerOptionText the text to be displayed after the descriptive Letter (A, B,...)
     */
    private void setAnswerOption(int buttonNumber, String answerOptionText) {
        String buttonLabel;
        switch (buttonNumber) {
            case 0:
                buttonLabel = "A: ";
                buttonLabel += answerOptionText;
                aButton.setText(buttonLabel);
                break;
            case 1:
                buttonLabel = "B: ";
                buttonLabel += answerOptionText;
                bButton.setText(buttonLabel);
                break;
            case 2:
                buttonLabel = "C: ";
                buttonLabel += answerOptionText;
                cButton.setText(buttonLabel);
                break;
            case 3:
                buttonLabel = "D: ";
                buttonLabel += answerOptionText;
                dButton.setText(buttonLabel);
                break;
        }
    }

    /**
     * Convenience Method to update all Button texts
     *
     * @param option1 Answer A text
     * @param option2 Answer B text
     * @param option3 Answer C text
     * @param option4 Answer D text
     */
    private void setAnswerOptions(String option1, String option2, String option3, String option4) {
        this.setAnswerOption(0, option1);
        this.setAnswerOption(1, option2);
        this.setAnswerOption(2, option3);
        this.setAnswerOption(3, option4);
    }

    /**
     * Convenience method to update the text of all Buttons.
     *
     * @param options Button texts, i.e. answers, in order A, B, C, D
     */
    private void setAnswerOptions(List<String> options) {
        for (int i = 0; i < options.size(); i++) {
            if (i < 4) {
                this.setAnswerOption(i, options.get(i));
            }
        }
    }

    /**
     * adds an ActionListener to all Buttons. Buttons should have an action command set, that the ActionLister can rely on
     *
     * @param actionListener the listener instance
     */
    public void addButtonListener(ActionListener actionListener) {
        this.aButton.addActionListener(actionListener);
        this.bButton.addActionListener(actionListener);
        this.cButton.addActionListener(actionListener);
        this.dButton.addActionListener(actionListener);
    }

    /**
     * can be called from outside to remove all content and show a message
     *
     * @param message the message to be displayed
     */
    public void showFinish(String message) {
        Container contentPane = this.getContentPane();
        contentPane.removeAll();
        JTextField messageArea = new JTextField(message);
        messageArea.setBorder(null);
        messageArea.setFont(new Font(Font.SANS_SERIF, Font.BOLD, 50));
        messageArea.setEditable(false);
        contentPane.add(messageArea, BorderLayout.CENTER);
        contentPane.revalidate();
    }

}
