package aufgabe01.controller;

import aufgabe01.model.Answer;
import aufgabe01.model.Game;
import aufgabe01.model.Question;
import aufgabe01.view.GUI;
import com.opencsv.CSVReader;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.*;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class GameController implements ActionListener {

    private static final String QUESTIONS_FILE_PATH = "assets" + File.separator + "questions.csv";

    private Game game;
    private Question currentQuestion;
    private GUI gui;

    public GameController(Game game) {
        this.game = game;
    }

    public GameController() {
        List<Question> questions = null;
        try {
            questions = readQuestionsFromFile(QUESTIONS_FILE_PATH);
        } catch (IOException e) {
            System.err.println("Es gab ein Problem beim Einlesen der Fragendatei. Bitte Existenz und Zugriffsrechte der Datei prüfen.");
            e.printStackTrace();
            System.exit(-1);
        }
        this.game = new Game(questions);
    }

    public void play(GUI reusableGui) {
        System.out.println("Starte...");

        if (reusableGui == null && this.gui == null) {
            this.gui = new GUI("Wer Wird Millionär");
        } else {
            this.gui = reusableGui;
        }
        gui.addButtonListener(this);
        gui.setVisible(true);
        this.proceed();
    }

    private void proceed() {
        if (!game.istGewonnen()) {
            this.currentQuestion = this.game.nextQuestion();
            gui.showQuestionAndAnswers(currentQuestion);
        } else {
            this.win();
        }
    }

    private List<Question> readQuestionsFromFile(String filename) throws FileNotFoundException, IOException {
        // FileReader zum Lesen der csv-Datei anlegen.
        Reader fileReader = new FileReader(filename);
        // CSVReader um den FileReader "wrappen". Trennungszeichen ist das Komma
        CSVReader csvReader = new CSVReader(fileReader, ',');
        // Eine Liste aus Zeilen (=String-Array) vom CSVReader zurückgeben lassen
        List<String[]> lines = csvReader.readAll();
        // Leere Liste zur Befüllung mit Fragen anlegen
        List<Question> questions = new ArrayList<Question>();
        // Durch alle Zeilen iterieren
        for (String[] line : lines) {
            // Erste Antwort ist die richtige Antwort
            Answer correctAnswer = new Answer(line[1]);
            // Alle weiteren Antworten kommen mit der Richtigen zusammen in die Sammlung
            List<Answer> allAnswers = new ArrayList<Answer>();
            allAnswers.add(correctAnswer);
            for (int index = 2; index < line.length; index++) {
                allAnswers.add(new Answer(line[index]));
            }
            // Frage erstellen
            questions.add(new Question(line[0], correctAnswer, allAnswers));
        }
        // Fragen mischen und zurückgeben
        Collections.shuffle(questions);
        return questions;
    }

    private void lose() {
        gui.showFinish("You Lose!");
    }

    private void win() {
        gui.showFinish("You Win!");
    }

    public static void main(String args[]) {
        GameController controller = new GameController();
        controller.play(null);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        String action = e.getActionCommand();
        int answerID = -1;
        if (action.equalsIgnoreCase(GUI.ACTION_A)) {
            answerID = 0;
        } else if (action.equalsIgnoreCase(GUI.ACTION_B)) {
            answerID = 1;
        } else if (action.equalsIgnoreCase(GUI.ACTION_C)) {
            answerID = 2;
        } else if (action.equalsIgnoreCase(GUI.ACTION_D)) {
            answerID = 3;
        }
        if (answerID <= this.currentQuestion.getAnswers().size() - 1) {
            Answer answerAttempt = this.currentQuestion.getAnswers().get(answerID);
            if (this.currentQuestion.isCorrectAnswer(answerAttempt)) {
                this.game.richtigBeantwortet();
                this.proceed();
            } else {
                this.lose();
            }
        } else if (answerID == -1) {
            this.lose();
        }

    }
}
